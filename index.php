<?php
	session_start();
	$con = mysqli_connect("localhost","root","ting1tong2ching3chong4","mdb");

	// Check connection
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mark Down Website</title>
	<link rel="stylesheet" href="/main.css">
</head>
<body>
	<h1>Markdown Browser</h1>

	<?php
		if(isset($_SESSION['domain']))
		{
	?>

	<h3>Editing domain: <?php echo $_SESSION['domain']; ?></h3>
	<nav>
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="/create">Create new page</a></li>
			<li><a href="/account">Account</a></li>
			<li><a href="/logout">Logout</a></li>
		</ul>
	</nav>

	<?php
		}
		else
		{
	?>
	<nav>
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="/signin">Sign in</a></li>
			<li><a href="/register">Register</a></li>
		</ul>
	</nav>

	<?php
		}
		if(isset($_GET['url']))
		{
			$url = explode('/',$_GET['url']);

			if(isset($_SESSION['domain']) || $url[0] == "register")
			{
				if(file_exists('./pages/'.$url[0].".php"))
				{
					include './pages/'.$url[0].'.php';
				}
				else
				{
					include './pages/home.php';
				}
			}
			else
			{
				include './pages/signin.php';
			}
		}
		else if(isset($_SESSION['domain']))
			include './pages/home.php';
		else
			include './pages/signin.php';

	?>
	<div class="download">
		<h2>Download the browser (linux only)</h2>
		<a href="./download/markdownbrowser.tar.gz" download>Download browser</a>
		<ol>
			<li>Click the download link.</li>
			<li>Extract all the files to your desktop.</li>
			<li>Open that folder and go to the dist folder.</li>
			<li>Double click the file in there.</li>
		</ol>
	</div>
</body>
</html>
