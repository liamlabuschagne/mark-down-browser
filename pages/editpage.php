<?php
	
	$domain = $_SESSION['domain']; 
	$page = mysqli_real_escape_string($con,$url[1]);
	// Get current content

	$sql = "SELECT content FROM pages WHERE domain = '$domain' AND name = '$page'";
	$result = mysqli_query($con,$sql);

	$content = "";

	while ($row = mysqli_fetch_assoc($result)) {
		$content = $row['content'];
	}

	if(isset($_POST['submit'])
	&& isset($_POST['content'])
	&& isset($_POST['page']))
	{
		$content = $_POST['content'];
		$page = $_POST['page'];

		$sql = "UPDATE pages SET content = '$content' WHERE domain = '$domain' AND name = '$page'";
		$result = mysqli_query($con,$sql);

		if(!$result){
			echo "Unable to submit content.";
		}
	}
?>
<h2>Edit</h2>
<h3>Editing page: <?php echo $page; ?></h3>
<form action="" method="post">
	<textarea name="content" cols="100" rows="10"><?php echo $content; ?></textarea>
	<br>
	<input type="submit" name="submit">
	<input type="hidden" name="page" value="<?php echo $page; ?>">
</form>