<?php
	
	$domain = $_SESSION['domain']; 
	$page = mysqli_real_escape_string($con,$url[1]);

	if(isset($_POST['submit'])
	&& isset($_POST['content'])
	&& isset($_POST['page']))
	{
		$content = mysqli_real_escape_string($con,$_POST['content']);
		$page = $_POST['page'];

		$sql = "UPDATE pages SET content = '$content' WHERE domain = '$domain' AND name = '$page'";
		$result = mysqli_query($con,$sql);

		if(!$result){
			echo "Unable to submit content.";
		}
	}

	// Get current content

	$sql = "SELECT content FROM pages WHERE domain = '$domain' AND name = '$page'";
	$result = mysqli_query($con,$sql);

	$content = "";

	while ($row = mysqli_fetch_assoc($result)) {
		$content = $row['content'];
	}
?>
<h2>Edit</h2>
<h3>Editing page: <?php echo $page; ?></h3>
<ul>
	<li>Heading 1: # [Heading text here]</li>
	<li>Heading 2: ## [Heading text here]</li>
	<li>Heading 3: ### [Heading text here]</li>
	<li>Heading 4: #### [Heading text here]</li>
	<li>Heading 5: ##### [Heading text here]</li>
	<li>Heading 6: ###### [Heading text here]</li>
	<li>Normal text: [just start typing]</li>
	<li>Link: @ [the path to the page eg: liam/about ]</li>
</ul>
<p>Note: The space is required and each one of these inputs need to be on seperate lines.</p>
<form action="" method="post">
	<textarea name="content" cols="100" rows="10"><?php echo $content; ?></textarea>
	<br>
	<input type="submit" name="submit" value="Save">
	<input type="hidden" name="page" value="<?php echo $page; ?>">
</form>