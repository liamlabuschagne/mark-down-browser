<?php
	$domain = $_SESSION['domain'];

	$page = $url[1];

	if(isset($_POST['submit'])
	&& isset($_POST['password']))
	{
		$password = mysqli_real_escape_string($con,$_POST['password']);

		// Get correct password from db
		$sql = "SELECT password FROM domains WHERE name = '$domain'";
		$result = mysqli_query($con,$sql);

		if($result)
		{
			$correct_password = mysqli_fetch_row($result)[0];
			if(password_verify($password, $correct_password)) {
				
				// Delete page
				$sql = "DELETE FROM pages WHERE name = '$page'";
				$result = mysqli_query($con,$sql);
				if($result)
					echo "Successfuly deleted page: ".$page;
			}
			else
			{
				echo "Incorrect password.";
			}
		}
	}
?>
<h2>Delete page: <?php echo $page; ?></h2>
<p>
	To confirm please type your domain password:
</p>
<form action="" method="post">
	<label for="password">Password:</label>
	<input type="password" name="password">
	<br>
	<input type="submit" name="submit">
</form>