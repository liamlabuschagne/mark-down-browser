<?php
	
	$domain = $_SESSION['domain'];

	if(isset($_POST['submit'])
	&& isset($_POST['page']))
	{
		$page = mysqli_real_escape_string($con,$_POST['page']);

		// Add the page
		$sql = "INSERT INTO pages (domain,name,content) VALUES ('$domain','$page','Default new page text.')";
		$result = mysqli_query($con,$sql);

		if($result)
		{
			header("Location: /edit/".$page);
		}
		else
		{
			echo "Page creation failed.";
		}
	}
?>
<h2>Create New Page:</h2>
<form action="" method="post">
	<label for="page">Choose a page name:</label>
	<input type="text" name="page">
	<br>
	<input type="submit" name="submit">
</form>
