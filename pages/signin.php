<?php
	if(isset($_POST['submit']) 
		&& isset($_POST['domain'])
		&& isset($_POST['password']))
	{
		$domain = mysqli_real_escape_string($con,$_POST['domain']);
		$password = mysqli_real_escape_string($con,$_POST['password']);

		// Get correct password from db
		$sql = "SELECT password FROM domains WHERE name = '$domain'";
		$result = mysqli_query($con,$sql);

		if($result)
		{
			$correct_password = mysqli_fetch_row($result)[0];
			if(password_verify($password, $correct_password)) {
				$_SESSION['domain'] = $domain;
				header("Location: /");
			}
			else
			{
				echo "Incorrect domain/password.";
			}
		}
		else
		{
			echo "Domain not found.";
		}
	}
?>
<h2>Sign in to a domain</h2>
<p>
	Don't have a domain? <a href="/register">Register one here</a>
</p>
<form action="" method="post">
	<label for="domain">Domain:</label>
	<input type="text" name="domain">
	<br>
	<label for="password">Password:</label>
	<input type="password" name="password">
	<input type="submit" name="submit" value="Sign in">
</form>
