<?php
	if(isset($_POST['submit']) 
		&& isset($_POST['password']))
	{
		$domain = $_SESSION['domain'];
		$password = mysqli_real_escape_string($con,$_POST['password']);

		// Get correct password from db
		$sql = "SELECT password FROM domains WHERE name = '$domain'";
		$result = mysqli_query($con,$sql);

		if($result)
		{
			$correct_password = mysqli_fetch_row($result)[0];
			if(password_verify($password, $correct_password)) {
				// Delete domain from domains table and all pages associated
				$sql = "DELETE FROM domains WHERE name = '$domain'";
				$result = mysqli_query($con,$sql);

				$successful = true;

				if(!$result)
					$successful = false;

				$sql = "DELETE FROM pages WHERE domain = '$domain'";
				$result = mysqli_query($con,$sql);

				if(!$result)
					$successful = false;

				if($successful)
				{
					header("Location: /logout");
				}
				else
				{
					echo "Unable to delete account/domain.";
				}

			}
			else
			{
				echo "Incorrect password.";
			}
		}
	}
?>
<h2>Account</h2>
<p>
	If you wish to delete your account/domain click the below button. After pressing this button there is no way to restore your domain or pages so be careful!
</p>
<h3>To confirm, type your domain password.</h3>
<form action="" method="post">
	<label for="password">Password:</label>
	<input type="password" name="password">
	<br>
	<input type="submit" name="submit">
</form>
