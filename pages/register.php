<?php
	if(isset($_POST['submit']) 
		&& isset($_POST['domain'])
		&& isset($_POST['password']))
	{
		$domain = mysqli_real_escape_string($con,$_POST['domain']);
		$password = mysqli_real_escape_string($con,$_POST['password']);

		// Check if domain exists
		$sql = "SELECT id FROM domains WHERE domain = '$domain'";
		$result = mysqli_query($con,$sql);

		if($result)
		{
			echo "Domain exits.";
		}

		$hashed_password = password_hash($password, PASSWORD_DEFAULT);

		// Add domain to database
		$sql = "INSERT INTO domains (name,password) VALUES ('$domain','$hashed_password')";
		$result = mysqli_query($con,$sql);

		if($result){
			$_SESSION['domain'] = $domain;
			// Add home page
			$sql = "INSERT INTO pages (domain,name,content) VALUES ('$domain','home','# Default home page')";
			$result = mysqli_query($con,$sql);

			header("Location: /");
		}

	}
?>
<h2>Register a domain</h2>
<p>
	Already have a domain? <a href="./sigin">Sign in here</a>
</p>
<form action="" method="post">
	<label for="domain">Choose a domain name:</label>
	<input type="text" name="domain">
	<br>
	<label for="password">Choose a password</label>
	<input type="password" name="password">
	<p>
		You cannot change this, so choose wisely and remember it.
	</p>
	<input type="submit" name="submit" value="Register">
</form>
